
module.exports = {
    'secret': process.env.SECRET_KEY,
    'mysqlUser': process.env.MYSQL_USER,
    'mysqlPwd': process.env.MYSQL_PWD,
    'mysqlDB': process.env.MYSQL_DB,
    'mysqlHost': process.env.MYSQL_HOST
}