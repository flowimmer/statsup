var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  bodyParser = require('body-parser');


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require('./api/routes/statsupRoutes');
routes(app);

var routes = require('./api/routes/authRoutes.js');
routes(app);


app.listen(port);

console.log('V0.0.0.1 RESTful API server started on: ' + port);
