
module.exports = function(app) {
  var authController = require('../controllers/authController');
  var statsup = require('../controllers/statsupController');

  app.route('/api/v1/nextStates')
    .get(authController.VerifyToken, statsup.getNextStates);

  app.route('/api/v1/setState')
    .post(authController.VerifyToken, statsup.authChannelToUser, statsup.authWriteAccess, statsup.setNextState);
    
    app.route('/api/v1/getCurrentState')
    .post(authController.VerifyToken, statsup.authChannelToUser, statsup.getCurrentState);

  app.route('/api/v1/generateToken') 
    .post(authController.VerifyToken, statsup.authChannelToUser, statsup.authAdminAccess, statsup.generateToken);

  app.route('/api/v1/useToken')
    .post(authController.VerifyToken, statsup.useToken);

  app.route('/api/v1/createChannel')
    .post(authController.VerifyToken, statsup.createChannel);

  app.route('/api/v1/getChannelsByUser')
    .post(authController.VerifyToken, statsup.getChannelsByUser);

  app.route("/api/v1/loadStatesOfChannel")
    .post(authController.VerifyToken, statsup.authChannelToUser, statsup.loadStatesOfChannel);

  app.route("/api/v1/createState")
    .post(authController.VerifyToken, statsup.authChannelToUser, statsup.authAdminAccess, statsup.addStateToChannel);

  app.route("/api/v1/loadChannelUser")
    .post(authController.VerifyToken, statsup.authChannelToUser, statsup.authAdminAccess, statsup.getChannelUser);

  app.route("/api/v1/retrieveRoles")
    .post(authController.VerifyToken, statsup.retrieveRoles);

  app.route("/api/v1/removeUser")
    .post(authController.VerifyToken, statsup.authChannelToUser, statsup.authAdminAccess, statsup.removeUser);

  app.route("/api/v1/deleteChannel")
    .post(authController.VerifyToken, statsup.authChannelToUser, statsup.authAdminAccess, statsup.deleteChannel);

  app.route("/api/v1/deleteState")
    .post(authController.VerifyToken, statsup.authChannelToUser, statsup.authAdminAccess, statsup.deleteState);
};
