

module.exports = function(app) {
    var authController = require('../controllers/authController');

    // var router = express.Router();
    // var bodyParser = require('body-parser');
    // router.use(bodyParser.urlencoded({ extended: false }));
    // router.use(bodyParser.json());

    app.route('/api/v1/register') 
        .post(authController.register);

    app.route('/api/v1/me')
        .get(authController.VerifyToken, authController.me);

    app.route('/api/v1/login')
        .post(authController.login);

    app.route('/api/v1/logout')
        .post(authController.logout);

    
};
  