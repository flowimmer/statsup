
var mysql = require('mysql');
var config = require('../../config');

var con = mysql.createConnection({
  host: config.mysqlHost,
  user: config.mysqlUser,
  password: config.mysqlPwd,
  database: config.mysqlDB
});

con.connect(function(err) {
    if (err) throw err;
});

exports.getNextStates = function(req, res)
{
    return res.status(404).send({succeed: false});
    con.query("CALL SelectNextPossibleStates(?)", [req.query.channelID], function (err, result, fields) {
        if (err) 
            return res.status(504).send({succeed: false});

        res.status(200).send(result[0]);
    });
}

exports.setNextState = function(req, res)
{
    con.query("CALL SetNextState(?,?)", [req.body.channelID, req.body.nextStateID], function(err, result, fields){
        if (err) 
        {
            return res.status(504).send({succeed: false});
        }
        else if (result[0][0].succeed === 0)
            return res.status(401).send({succeed: false, msg: "Statetransaction not Possible"});
        else
        {

            con.query("CALL GetCurrentState(?)", [req.body.channelID], function(err, result, fields){
                if (err) 
                {
                    return res.status(504).send({succeed: true});
                }

                res.status(200).send({succeed: true, result: result[0][0]});
            });
        }
    });
}

exports.getCurrentState = function(req, res)
{
    con.query("CALL GetCurrentState(?)", [req.body.channelID], function(err, result, fields){
        if (err) 
            return res.status(504).send({succeed: false});

        res.status(200).send({succeed: true, result: result[0][0]});
    });
}

exports.generateToken = function(req, res)
{
    con.query("CALL CreateToken(?,?,?)", [req.body.channelID, req.body.roleID, req.userID], function(err, result, fields){
        if (err) 
            return res.status(504).send({succeed: false, err: err});
            res.status(200).send({succeed: true, inviteToken: result[0][0].Token, roleName: result[0][0].RoleName});
    });
}

exports.useToken = function(req, res)
{
    con.query("CALL ValidateToken(?,?)", [req.body.Token, req.userID], function(err, result, fields){
        if (err) 
            return res.status(504).send({succeed: false, err: err});

        res.status(200).send({succeed: true});
    });
}

exports.authChannelToUser = function(req, res, next)
{
    var channelID;
    if(req.body.channelID == undefined)
        channelID = req.query.channelID;
    else
        channelID = req.body.channelID;
    con.query("CALL UserAttendsChannel(?,?)", [req.userID, channelID], function(err, result, fields){
        if(err || result[0].length == 0)
            return res.status(403).send("You dont attend the channel.");
        
        req.roleID = result[0][0].Role_ID;
        next();
    });
    
}

exports.authWriteAccess = function(req, res, next)
{
    if(req.roleID <= 2)
    {
        next();
    }
    else
    {
        res.status(200).send({succeed: false, msg: "You don't have the rights for this"})
    }
}

exports.authAdminAccess = function(req, res, next)
{
    if(req.roleID == 1)
    {
        next();
    }
    else
    {
        res.status(200).send({succeed: false, msg: "You don't have the rights for this"})
    }
}


exports.getChannelsByUser = function(req, res)
{
    con.query("CALL ChannelsOfUser(?)", [req.userID], function(err, result, fields){
        if(err)
            return res.status(504).send({succeed: false})

        res.status(200).send({succeed: true, result:result[0]});
    });
}

exports.createChannel = function(req, res)
{
    con.query("START TRANSACTION");
    con.query("CALL CreateChannel(?,?,?)", [req.body.name, req.body.description, req.userID], function(err, result, fields)
    {
        console.log(err);
        if(err)
        {
            con.query("ROLLBACK");
            return res.status(504).send({succeed: false, err: err});
        }

        con.query("COMMIT");
        
        res.status(200).send({succeed: true});
    });
}


exports.loadStatesOfChannel = function(req, res)
{
    con.query("CALL GetStatesOfChannel(?)", [req.body.channelID], function(err, result, fields){
        if(err)
        {
            return res.status(504).send({succeed: false, err: err});
        }
        res.status(200).send({succeed: true, result:result[0]});
    });
}

exports.addStateToChannel = function(req, res)
{
    con.query("START TRANSACTION");
    con.query("CALL AddStateToChannel(?,?,?)", [req.body.name, req.body.description, req.body.channelID], function(err, result, fields){
        if(err)
        {
            con.query("ROLLBACK");
            return res.status(504).send({succeed: false, err: err});
        }
        con.query("COMMIT");

        res.status(200).send({succeed: true});
    });
}

exports.getChannelUser = function(req, res)
{
    con.query("CALL SelectUserFromChannel(?)", [req.body.channelID], function(err, result, fields){
        if(err)
        {
            return res.status(504).send({succeed: false, err: err});
        }
        var user = result[0];
        
        con.query("CALL GetMyTokensForChannel(?,?)", [req.body.channelID, req.userID], function(err, result, fields){
            console.log(result);
            if(err)
            {
                return res.status(504).send({succeed: false, err: err});
            }
            
            res.status(200).send({succeed: true, user: user, tokens: result[0]});
        });
      
    });
    
}

exports.retrieveRoles = function(req, res)
{
    con.query("SELECT * FROM Role", function(err, result, fields){
        if(err)
        {
            return res.status(504).send({succeed: false, err: err});
        }
        res.status(200).send({succeed: true, result: result});
    });
}

exports.removeUser = function(req, res)
{
    con.query("DELETE FROM participate WHERE User_ID = ? AND Channel_ID = ?", [req.body.userID, req.body.channelID], function(err, result, fields){
        if(err)
        {
            return res.status(504).send({succeed: false, err: err});
        }
        res.status(200).send({succeed: true});
    });
}

exports.deleteChannel = function(req, res)
{
    con.query("DELETE FROM Channel WHERE ID = ?", [req.body.channelID], function(err, result, fields){
        if(err)
        {
            return res.status(504).send({succeed: false, err: err});
        }
        res.status(200).send({succeed: true});
    });
}

exports.deleteState = function(req, res)
{
    con.query("DELETE FROM State WHERE ID = ?", [req.body.stateID], function(err, result, fields){
        if(err)
        {
            return res.status(504).send({succeed: false, err: err});
        }
        res.status(200).send({succeed: true});
    });
}