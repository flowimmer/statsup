
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../../config');

var mysql = require('mysql');

var con = mysql.createConnection({
    host: config.mysqlHost,
    user: config.mysqlUser,
    password: config.mysqlPwd,
    database: config.mysqlDB
  });

con.connect(function(err) {
    if (err) throw err;
});

exports.register = function(req, res)
{
    var hashedPassword = bcrypt.hashSync(req.body.password, 8);

    con.query("CALL CreateUser(?,?,?)", [req.body.name, req.body.email, hashedPassword], function (err, result, fields) {
        if (err)
        {
            console.log(err);
            return res.status(500).send({succeed: false, msg: "Unable to register this user."});
        }
        
        var userID = result[0][0].ID;
        // create a token
        var token = jwt.sign({ id: userID}, config.secret, {
            expiresIn: 43200
        });

        res.status(200).send({auth: true, token: token});
    });
}

exports.VerifyToken = function(req, res, next)
{
    var token = req.headers['x-access-token'];
    if (!token) 
        return res.status(401).send({succeed: false, auth: false, message: 'No token provided.' });
  
    jwt.verify(token, config.secret, function(err, decoded) {
    if (err) 
        return res.status(500).send({succeed: false, auth: false, message: 'Failed to authenticate token.' });
    
    con.query("SELECT Name FROM User WHERE ID = ?", [decoded.id], function (err, result, fields) {
        if (err)
        {
            console.log(err);
            return res.status(500).send({succeed: false, auth: false, message: 'Failed to authenticate token.' });
        }
        else if(result.length === 0)
        {
            return res.status(500).send({succeed: false, auth: false, message: 'Failed to authenticate token.' });
        }
        var user = result[0].Name;

        //res.status(200).send({username: user});
        req.userID = decoded.id;
        next();
    });
  });
}

exports.me = function(req, res, next)
{
    res.status(200).send("OK");
}


exports.login = function(req, res)
{
    con.query("SELECT Password, ID, Name FROM User WHERE email = ?", [req.body.email], function (err, result, fields) {
        if (err)
        {
            return res.status(500).send("There was a problem during the login.");
        }
        if (result.length === 0)
        {
            return res.status(401).send({succeed: false, auth: false, token: null });
        }
        
        var pwd = result[0].Password;
        var userID = result[0].ID;
        var userName = result[0].Name;

        var passwordIsValid = bcrypt.compareSync(req.body.password, pwd);
        if (!passwordIsValid) 
            return res.status(401).send({succeed: false, auth: false, token: null });


        // create a token
        var token = jwt.sign({id: userID}, config.secret, {
            expiresIn: 86400
        });

        res.status(200).send({succeed: true, auth: true, token: token, name: userName});
    });
}

exports.logout = function(req, res)
{
    res.status(200).send({succeed: true, auth: false, token: null, msg: ""});
}